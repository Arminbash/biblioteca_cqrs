﻿using BibliotecaWeb.Application.Interfaces;
using System.Data.Entity;
using BibliotecaWeb.Persistence.Configurations;

namespace BibliotecaWeb.Persistence
{
    public class BibliotecaDbContext : DbContext, IBibliotecaDbContext
    {
        public BibliotecaDbContext() : base("BibliotecaConexion")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AutorConfiguration());
            modelBuilder.Configurations.Add(new EstudianteConfiguration());
            modelBuilder.Configurations.Add(new LibroConfiguration());
            modelBuilder.Configurations.Add(new PrestamoConfiguration());
            modelBuilder.Configurations.Add(new LibAutConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
