﻿using BibliotecaWeb.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BibliotecaWeb.Persistence.Configurations
{
    public class PrestamoConfiguration : EntityTypeConfiguration<Prestamo>
    {
        public PrestamoConfiguration()
        {
            this.ToTable("Prestamo");
            this.HasKey(e => e.IdPrestamo);

            this.HasRequired(d => d.Estudiante)
                .WithMany(e => e.Prestamos)
                .HasForeignKey(k => k.IdLector);

            this.HasRequired(d => d.Libro)
                .WithMany(e => e.Prestamos)
                .HasForeignKey(k => k.IdLibro);
                
        }
    }
}
