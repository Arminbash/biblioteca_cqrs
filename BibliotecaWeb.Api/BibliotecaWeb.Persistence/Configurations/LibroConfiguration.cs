﻿using BibliotecaWeb.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BibliotecaWeb.Persistence.Configurations
{
    public class LibroConfiguration : EntityTypeConfiguration<Libro>
    {
        public LibroConfiguration()
        {
            this.ToTable("Libro");
            this.HasKey(e => e.IdLibro);
        }
    }
}
