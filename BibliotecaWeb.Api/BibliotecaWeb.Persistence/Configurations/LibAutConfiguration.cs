﻿using BibliotecaWeb.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BibliotecaWeb.Persistence.Configurations
{
    public class LibAutConfiguration : EntityTypeConfiguration<LibAut>
    {
        public LibAutConfiguration()
        {
            this.ToTable("LibAut");
            this.HasKey(e => e.IdLibAut);

            this.HasRequired(d => d.Autor)
                .WithMany(e => e.LibAuts)
                .HasForeignKey(k => k.IdAutor);

            this.HasRequired(d => d.Libro)
                .WithMany(e => e.LibAuts)
                .HasForeignKey(k => k.IdLibro);
        }
    }
}
