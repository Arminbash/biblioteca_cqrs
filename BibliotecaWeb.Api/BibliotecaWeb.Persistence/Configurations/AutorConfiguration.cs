﻿using BibliotecaWeb.Domain.Entities;
using EntityFramework.Metadata;
using EntityFramework;
using System;
using System.Data.Entity.ModelConfiguration;

namespace BibliotecaWeb.Persistence.Configurations
{
    public class AutorConfiguration : EntityTypeConfiguration<Autor>
    {
        public AutorConfiguration()
        {
            this.ToTable("Autor");
            this.HasKey(e => e.IdAutor);
        }
    }
}
