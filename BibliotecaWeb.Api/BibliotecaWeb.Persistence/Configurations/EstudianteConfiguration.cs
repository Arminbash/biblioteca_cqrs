﻿using BibliotecaWeb.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace BibliotecaWeb.Persistence.Configurations
{
    public class EstudianteConfiguration : EntityTypeConfiguration<Estudiante>
    {
        public EstudianteConfiguration()
        {
            this.ToTable("Estudiante");
            this.HasKey(e => e.IdLector);
        }
    }
}
