﻿using System.Collections.Generic;

namespace BibliotecaWeb.Domain.Entities
{
    public class Autor
    {
        public Autor()
        {
            LibAuts = new HashSet<LibAut>();
        }
        public int IdAutor { get; set; }
        public string Nombre { get; set; }
        public string Nacionalidad { get; set; }

        public ICollection<LibAut> LibAuts { get; private set; }
    }
}