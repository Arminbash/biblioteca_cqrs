﻿using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace BibliotecaWeb.Application.Interfaces
{
    public interface IBibliotecaDbContext
    {
        DbSet<T> Set<T>() where T : class;
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
