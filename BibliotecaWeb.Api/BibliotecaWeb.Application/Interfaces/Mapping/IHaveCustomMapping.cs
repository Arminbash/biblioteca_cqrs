﻿using AutoMapper;
namespace BibliotecaWeb.Application.Interfaces.Mapping
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}
