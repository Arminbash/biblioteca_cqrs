﻿using BibliotecaWeb.Application.Interfaces;
using BibliotecaWeb.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BibliotecaWeb.Application.Execution.Commands
{
    public class CreateAutor : IRequest, IService
    {
        public Autor autor { get; set; }
        public class Handler : IRequestHandler<CreateAutor, Unit>
        {
            private readonly IBibliotecaDbContext _context;
            private readonly IMediator _mediator;

            public Handler(IBibliotecaDbContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<Unit> Handle(CreateAutor request, CancellationToken cancellationToken)
            {
                var entity = new Autor
                {
                    IdAutor = request.autor.IdAutor,
                    Nombre = request.autor.Nombre,
                    Nacionalidad = request.autor.Nacionalidad
                };

                _context.Set<Autor>().Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }


    }
}
