﻿using BibliotecaWeb.Application.Execution.Commands;
using BibliotecaWeb.Application.Infrastructure;
using BibliotecaWeb.Application.Interfaces;
using BibliotecaWeb.Domain.Entities;
using BibliotecaWeb.Domain.Models;
using MediatR;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BibliotecaWeb.Services
{
    public class AutorService
    {
        private readonly IBibliotecaDbContext _context;
        private readonly IMediator _mediator;
        //public AutorService(IBibliotecaDbContext context, IMediator mediator)
        //{
        //    _context = context;
        //    _mediator = mediator;
        //}
        public AutorService(IBibliotecaDbContext context)
        {
            _context = context;
            _mediator = new Mediator(BuildMediator.Build(CreateAutor));
        }
        public bool crearAutor(AutorViewModel autorView)
        {
            try
            {
                var mediatorMock = new Mock<IMediator>();
                var sut = new CreateAutor.Handler(_context, mediatorMock.Object);

                Autor autor = new Autor()
                {
                    Nombre = autorView.Nombre,
                    Nacionalidad = autorView.Nacionalidad
                };

                // Act
                var result = sut.Handle(new CreateAutor {autor = autor } , CancellationToken.None);

                return true;
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
    }
}
